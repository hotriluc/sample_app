module UsersHelper
    
    
    def gravatar_for(user, options = {size: 80})
        
        size = options[:size]
        # Getting gravatar id by email
        gravatar_id = Digest::MD5::hexdigest(user.email.downcase)
        
        # getting gravatar url by using digest of user email (gravatar id)
        gravatar_url = "https://secure.gravatar.com/avatar/#{gravatar_id}?s=#{size}"
        image_tag(gravatar_url, alt: user.name, class: "gravatar")
        
        
    end
    
end
