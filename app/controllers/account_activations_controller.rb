class AccountActivationsController < ApplicationController
    include SessionsHelper
    
    
    def edit
       
       # Get user by email
       user = User.find_by(email: params[:email])
       
       
       if user && !user.activated? && user.authenticated?(:activation, params[:id])
           
            # The code bellow will be executed when user's activation digest equeals to activation token provided by the link
            # also user supposed to be not activated
            # user.update_attribute(:activated,    true)
            # user.update_attribute(:activated_at, Time.zone.now)
            user.activate
            log_in user
            flash[:success] = "Account activated!"
            redirect_to user
        else
            # This part will be executed if user has already activated his account
            flash[:danger] = "Invalid activation link"
            redirect_to root_url
        end
    end
end
    

