require 'test_helper'

class UsersSignupTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end
  
  
  def setup
     ActionMailer::Base.deliveries.clear
  end
  
  
  test "invalid sign up information" do
    get signup_path
    
    
    # Counting users before sending form (invalid user info)
    # test is passed if invalid info won't be saved into the DB
    # It means User.count before equals to User.count after sending form
    assert_no_difference "User.count" do
      post users_path, user: {
        name: "",
        email: "user@invalid",
        password: "foo",
        password_confirmation: "bar"
        
      }
      
    end
    
    assert_template "users/new"
    assert_select 'div#error_explanation'
    assert_select 'div.field_with_errors'
  end
  
  
  test "valid sign up information with activation" do
    get signup_path
    
    assert_difference "User.count", 1 do
      post users_path, user: {
          name: "Example User",
          email: "user@example.com",
          password: "password",
          password_confirmation: "password"
      }
      
    end
    
    
    
    # check if only one message has been delivered 
    assert_equal 1, ActionMailer::Base.deliveries.size
    
    # access to @user by using assigns
    user = assigns(:user)
    
    # user is not activated then pass
    assert_not user.activated?
    
    # Попытка войти до активации
    log_in_as(user)
    assert_not is_logged_in?
    
    # Невалидный активационный токен
    get edit_account_activation_path("invalid token")
    assert_not is_logged_in?
    
    # Валидный токен, неверный адрес электронной почты
    get edit_account_activation_path(user.activation_token, email: 'wrong')
    assert_not is_logged_in?
    
    # Валидный активационный токен и адрес почты
    get edit_account_activation_path(user.activation_token, email: user.email)
    assert user.reload.activated?
    
    # Successful activation following with redirection
    follow_redirect!
    assert_template 'users/show'
    assert is_logged_in?
    
  end
  
  
end

